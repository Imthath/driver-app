package com.appsoft.admin.service.service.impl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.CustomProduct;
import com.appsoft.admin.service.model.Product;
import com.appsoft.admin.service.repository.CustomProductRepository;
import com.appsoft.admin.service.repository.ProductRepository;
import com.appsoft.admin.service.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService{
	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public Product getProductByProductName(String productName) throws ResourceNotFoundException {
		com.appsoft.admin.service.entity.Product productEntity = productRepository.findByProductName(productName);
		if (productEntity == null) {
			throw new ResourceNotFoundException("Product not found : " + productName);
		} else {
			return new Product(productEntity.getProductId(), productEntity.getProductName(), productEntity.getPrice(),
					productEntity.getUnitType(), productEntity.getQuantity(), productEntity.getBrand(),
					productEntity.getDiscount(), productEntity.getIsWholeSale(), productEntity.getUserId(),
					productEntity.getUpdatedDateTime());
		}
	}

	@Override
	public Product getProductByProductId(Integer productId) throws ResourceNotFoundException {
		com.appsoft.admin.service.entity.Product productEntity = productRepository.findByProductId(productId);
		if (productEntity == null) {
			throw new ResourceNotFoundException("Product not found : " + productId);
		} else {
			return new Product(productEntity.getProductId(), productEntity.getProductName(), productEntity.getPrice(),
					productEntity.getUnitType(), productEntity.getQuantity(), productEntity.getBrand(),
					productEntity.getDiscount(), productEntity.getIsWholeSale(), productEntity.getUserId(),
					productEntity.getUpdatedDateTime());
		}
	}

	@Override
	public Map<String, Object> getAllProducts(Integer pageNo, Integer pageSize, String sortBy)
			throws ResourceNotFoundException {
		Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
		Page<com.appsoft.admin.service.entity.Product> productEntities = productRepository.findAll(pageable);

		if (productEntities.hasContent()) {
			Map<String, Object> response = new HashMap<>();
			response.put("total", productEntities.getTotalElements());
			response.put("products", productEntities.getContent().stream()
					.map(productEntity -> new Product(productEntity.getProductId(), productEntity.getProductName(), productEntity.getPrice(),
							productEntity.getUnitType(), productEntity.getQuantity(), productEntity.getBrand(),
							productEntity.getDiscount(), productEntity.getIsWholeSale(), productEntity.getUserId(),
							productEntity.getUpdatedDateTime()))
					.collect(Collectors.toList()));
			return response;

		} else {
			throw new ResourceNotFoundException("No any User found");
		}
	}

	@Override
	public HttpStatus updateProduct(Product product) throws ResourceNotFoundException, Exception {
		Optional<com.appsoft.admin.service.entity.Product> productEntityOpt = productRepository.findById(product.getProductId());
		if (!productEntityOpt.isPresent()) {
			throw new ResourceNotFoundException("Product not found : " + product.getProductName());
		} else {
			try {
				com.appsoft.admin.service.entity.Product productEntity = productEntityOpt.get();
				productEntity.setProductName(product.getProductName());
				productEntity.setPrice(product.getPrice());
				productEntity.setUnitType(product.getUnitType());
				productEntity.setQuantity(product.getQuantity());
				productEntity.setBrand(product.getBrand());
				productEntity.setDiscount(product.getDiscount());
				productEntity.setIsWholeSale(product.getIsWholeSale());
				productEntity.setUserId(product.getUserId());
				productEntity.setUpdatedDateTime(product.getUpdatedDateTime());

				productRepository.save(productEntity);

				return HttpStatus.OK;
			} catch (Throwable e) {
				throw new Exception("Internal server error");
			}
		}
	}

	@Override
	public HttpStatus deleteProduct(Product product) throws ResourceNotFoundException, Exception {
		Optional<com.appsoft.admin.service.entity.Product> productEntityOpt = productRepository.findById(product.getProductId());
		if (!productEntityOpt.isPresent()) {
			throw new ResourceNotFoundException("Product not found : " + product.getProductName());
		} else {
			try {
				productRepository.delete(productEntityOpt.get());
				return HttpStatus.OK;
			} catch (Throwable e) {
				throw new Exception("Internal server error");
			}
		}
	}

	@Override
	public HttpStatus createProduct(Product product) {
		com.appsoft.admin.service.entity.Product productEntity = productRepository.findByProductName(product.getProductName());
		if (productEntity == null) {
			productEntity = new com.appsoft.admin.service.entity.Product(product.getProductName());
			productEntity.setPrice(product.getPrice());
			productEntity.setUnitType(product.getUnitType());
			productEntity.setQuantity(product.getQuantity());
			productEntity.setBrand(product.getBrand());
			productEntity.setDiscount(product.getDiscount());
			productEntity.setIsWholeSale(product.getIsWholeSale());
			productEntity.setUserId(product.getUserId());
			productEntity.setUpdatedDateTime(product.getUpdatedDateTime());

			productRepository.save(productEntity);

			return HttpStatus.OK;
		} else {
			return HttpStatus.CONFLICT;
		}
	}
	
}
