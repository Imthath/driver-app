package com.appsoft.admin.service.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.appsoft.admin.service.entity.Store;

public interface StoreRepository extends PagingAndSortingRepository<Store, Integer> {
	Store findByStoreId(Integer storeId);
	
	Store findByStoreName(String storeName);
}
