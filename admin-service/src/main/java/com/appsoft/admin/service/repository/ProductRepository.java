package com.appsoft.admin.service.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.appsoft.admin.service.entity.Product;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, Integer> {
	Product findByProductId(Integer productId);

	Product findByProductName(String productNane);
}
