package com.appsoft.admin.service.model;

import java.io.Serializable;
import java.sql.Date;

import com.appsoft.admin.service.entity.Payment;

public class Sale implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer saleId;		
	private Integer productId;	
	private String productName;	
	private Double price;	
	private Double quantity;	
	private Double amount;	
	private Double discount;	
	private Double netTotal;	
	private String posFlag;	
	private Date saleTime;
	private Integer userId;
    private Payment payment;
    
    public Sale(Integer saleId, Integer productId, String productName, Double price, Double quantity, Double amount,
			Double discount, Double netTotal, String posFlag, Date saleTime, Integer userId, Payment payment) {
		this.setSaleId(saleId);
		this.setProductId(productId);
		this.setProductName(productName);
		this.setPrice(price);
		this.setQuantity(quantity);
		this.setAmount(amount);
		this.setDiscount(discount);
		this.setNetTotal(netTotal);
		this.setPosFlag(posFlag);
		this.setSaleTime(saleTime);
		this.setUserId(userId);
		this.setPayment(payment);
	}
    
    public Sale() {
    }
    
	public Integer getSaleId() {
		return saleId;
	}
	public void setSaleId(Integer saleId) {
		this.saleId = saleId;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public Double getNetTotal() {
		return netTotal;
	}
	public void setNetTotal(Double netTotal) {
		this.netTotal = netTotal;
	}
	public String getPosFlag() {
		return posFlag;
	}
	public void setPosFlag(String posFlag) {
		this.posFlag = posFlag;
	}
	public Date getSaleTime() {
		return saleTime;
	}
	public void setSaleTime(Date saleTime) {
		this.saleTime = saleTime;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Payment getPayment() {
		return payment;
	}
	public void setPayment(Payment payment) {
		this.payment = payment;
	}
}
