package com.appsoft.admin.service.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.Return;
import com.appsoft.admin.service.repository.ReturnRepository;
import com.appsoft.admin.service.service.ReturnService;

@Service
public class ReturnServiceImpl implements ReturnService{
	@Autowired
	private ReturnRepository returnRepository;
	@Override
	public Return getReturnByProductName(String productName) throws ResourceNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Return getReturnByReturnId(Integer returnId) throws ResourceNotFoundException {
		com.appsoft.admin.service.entity.Return returnEntity = returnRepository.findByReturnId(returnId);
		if (returnEntity == null) {
			throw new ResourceNotFoundException("Return not found : " + returnId);
		} else {
			return new Return(returnEntity.getReturnId(), returnEntity.getProductId(), returnEntity.getPrice(),
					returnEntity.getQuantity(), returnEntity.getAmount(), returnEntity.getDiscount(),
					returnEntity.getNetTotal(), returnEntity.getResonForReturn(), returnEntity.getUserId(),
					returnEntity.getReturnTime());
		}
	}

	@Override
	public Map<String, Object> getAllReturn(Integer pageNo, Integer pageSize, String sortBy)
			throws ResourceNotFoundException {
		Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
		Page<com.appsoft.admin.service.entity.Return> returnEntities = returnRepository.findAll(pageable);

		if (returnEntities.hasContent()) {
			Map<String, Object> response = new HashMap<>();
			response.put("total", returnEntities.getTotalElements());
			response.put("returns", returnEntities.getContent().stream()
					.map(returnEntity -> new Return(returnEntity.getReturnId(), returnEntity.getProductId(), returnEntity.getPrice(),
							returnEntity.getQuantity(), returnEntity.getAmount(), returnEntity.getDiscount(),
							returnEntity.getNetTotal(), returnEntity.getResonForReturn(), returnEntity.getUserId(),
							returnEntity.getReturnTime()))
					.collect(Collectors.toList()));
			return response;

		} else {
			throw new ResourceNotFoundException("No any Return found");
		}
	}

	@Override
	public HttpStatus updateReturn(Return return1) throws ResourceNotFoundException, Exception {
		Optional<com.appsoft.admin.service.entity.Return> returnEntityOpt = returnRepository.findById(return1.getReturnId());
		if (!returnEntityOpt.isPresent()) {
			throw new ResourceNotFoundException("Return not found : " + return1.getReturnId());
		} else {
			try {
				com.appsoft.admin.service.entity.Return returnEntity = returnEntityOpt.get();
				returnEntity.setProductId(return1.getProductId());
				returnEntity.setPrice(return1.getPrice());
				returnEntity.setQuantity(return1.getQuantity());
				returnEntity.setAmount(return1.getAmount());
				returnEntity.setDiscount(return1.getDiscount());
				returnEntity.setNetTotal(return1.getNetTotal());
				returnEntity.setResonForReturn(return1.getResonForReturn());
				returnEntity.setUserId(return1.getUserId());
				returnEntity.setReturnTime(return1.getReturnTime());
	
				returnRepository.save(returnEntity);

				return HttpStatus.OK;
			} catch (Throwable e) {
				throw new Exception("Internal server error");
			}
		}
	}

	@Override
	public HttpStatus deleteReturn(Return return1) throws ResourceNotFoundException, Exception {
		Optional<com.appsoft.admin.service.entity.Return> returnEntityOpt = returnRepository.findById(return1.getReturnId());
		if (!returnEntityOpt.isPresent()) {
			throw new ResourceNotFoundException("Return not found : " + return1.getReturnId());
		} else {
			try {
				returnRepository.delete(returnEntityOpt.get());
				return HttpStatus.OK;
			} catch (Throwable e) {
				throw new Exception("Internal server error");
			}
		}
	}

	@Override
	public HttpStatus createReturn(Return return1) {
		com.appsoft.admin.service.entity.Return returnEntity = returnRepository.findByReturnId(return1.getReturnId());
		if (returnEntity == null) {
			returnEntity = new com.appsoft.admin.service.entity.Return(return1.getReturnId());
			returnEntity.setPrice(return1.getPrice());
			returnEntity.setQuantity(return1.getQuantity());
			returnEntity.setAmount(return1.getAmount());
			returnEntity.setDiscount(return1.getDiscount());
			returnEntity.setNetTotal(return1.getNetTotal());
			returnEntity.setResonForReturn(return1.getResonForReturn());
			returnEntity.setUserId(return1.getUserId());
			returnEntity.setReturnTime(return1.getReturnTime());

			returnRepository.save(returnEntity);

			return HttpStatus.OK;
		} else {
			return HttpStatus.CONFLICT;
		}
	}

}
