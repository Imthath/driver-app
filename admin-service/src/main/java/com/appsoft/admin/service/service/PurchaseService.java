package com.appsoft.admin.service.service;

import java.util.Map;

import org.springframework.http.HttpStatus;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.Purchase;

public interface PurchaseService {
	Purchase getPurchaseByPurchaseId(Integer purchase) throws ResourceNotFoundException;

	Map<String, Object> getAllPurchase(Integer pageNo, Integer pageSize, String sortBy) throws ResourceNotFoundException;

	HttpStatus updatePurchase(Purchase purchase) throws ResourceNotFoundException, Exception;

	HttpStatus deletePurchase(Purchase purchase) throws ResourceNotFoundException, Exception;

	HttpStatus createPurchase(Purchase purchase);
}
