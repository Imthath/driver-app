package com.appsoft.admin.service.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.entity.CustomProduct;
import com.appsoft.admin.service.service.CustomProductService;

@RestController
@RequestMapping(value = "/customProduct/v1")
public class CustomProductController {

	@Autowired
	CustomProductService customProductService;
	
	@GetMapping("/customProduct")
    public ResponseEntity<?> findCitiesNameEndsWith(@RequestParam String productName)  throws ResourceNotFoundException, Exception {

        return new ResponseEntity<List<CustomProduct>>(customProductService.findByNameEndsWith(productName),
				HttpStatus.OK);
    }
}
