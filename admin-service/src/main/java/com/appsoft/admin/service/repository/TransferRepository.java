package com.appsoft.admin.service.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.appsoft.admin.service.entity.Transfer;

public interface TransferRepository extends PagingAndSortingRepository<Transfer, Integer> {
	Transfer findByTransferId(Integer transferId);
}
