package com.appsoft.admin.service.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.appsoft.admin.service.entity.Return;

public interface ReturnRepository extends PagingAndSortingRepository<Return, Integer> {
	Return findByReturnId(Integer returnId);
}
