package com.appsoft.admin.service.model;

import java.io.Serializable;

public class Installement implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer installementId;
	private Integer productId;
	private Double initialPayment;
	private Double monthlyPercentage;
	private Integer paymentId;
	private String installementStatus;
	
	public Installement(Integer installementId, Integer productId, Double InitialPayment,
			Double monthlyPercentage, Integer paymentId, String instalementStatus) {
		this.setInstallementId(installementId);
		this.setProductId(productId);
		this.setInitialPayment(InitialPayment);
		this.setMonthlyPercentage(monthlyPercentage);
		this.setPaymentId(paymentId);
		this.setInstallementStatus(instalementStatus);
	}
	
	public Installement() {
		
	}
	public Integer getInstallementId() {
		return installementId;
	}
	public void setInstallementId(Integer installementId) {
		this.installementId = installementId;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public Double getInitialPayment() {
		return initialPayment;
	}
	public void setInitialPayment(Double initialPayment) {
		this.initialPayment = initialPayment;
	}
	public Double getMonthlyPercentage() {
		return monthlyPercentage;
	}
	public void setMonthlyPercentage(Double monthlyPercentage) {
		this.monthlyPercentage = monthlyPercentage;
	}
	public Integer getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(Integer paymentId) {
		this.paymentId = paymentId;
	}
	public String getInstallementStatus() {
		return installementStatus;
	}
	public void setInstallementStatus(String installementStatus) {
		this.installementStatus = installementStatus;
	}

}
