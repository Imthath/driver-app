package com.appsoft.admin.service.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.Payment;
import com.appsoft.admin.service.repository.PaymentRepository;
import com.appsoft.admin.service.service.PaymentService;

@Service
public class PaymentServiceImpl implements PaymentService{
	@Autowired
	private PaymentRepository paymentRepository;
	@Override
	public Payment getPaymentByPaymentId(Integer paymentId) throws ResourceNotFoundException {
		com.appsoft.admin.service.entity.Payment paymentEntity = paymentRepository.findByPaymentId(paymentId);
		if (paymentEntity == null) {
			throw new ResourceNotFoundException("Payment not found : " + paymentId);
		} else {
			return new Payment(paymentEntity.getPaymentId(), paymentEntity.getNetAmount(), paymentEntity.getPaymentMethod(), paymentEntity.getPaymentTime(),
					paymentEntity.getUserId(), paymentEntity.getPaymentType(), paymentEntity.getSaleList());
		}
	}

	@Override
	public Map<String, Object> getAllPayments(Integer pageNo, Integer pageSize, String sortBy)
			throws ResourceNotFoundException {
		Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
		Page<com.appsoft.admin.service.entity.Payment> paymentEntities = paymentRepository.findAll(pageable);

		if (paymentEntities.hasContent()) {
			Map<String, Object> response = new HashMap<>();
			response.put("total", paymentEntities.getTotalElements());
			response.put("payments", paymentEntities.getContent().stream()
					.map(paymentEntity -> new Payment(paymentEntity.getPaymentId(), paymentEntity.getNetAmount(), paymentEntity.getPaymentMethod(), paymentEntity.getPaymentTime(),
							paymentEntity.getUserId(), paymentEntity.getPaymentType(), paymentEntity.getSaleList()))
					.collect(Collectors.toList()));
			return response;

		} else {
			throw new ResourceNotFoundException("No any Payment found");
		}
	}

	@Override
	public HttpStatus updatePayment(Payment payment) throws ResourceNotFoundException, Exception {
		Optional<com.appsoft.admin.service.entity.Payment> paymentEntityOpt = paymentRepository.findById(payment.getPaymentId());
		if (!paymentEntityOpt.isPresent()) {
			throw new ResourceNotFoundException("Payment not found : " + payment.getPaymentId());
		} else {
			try {
				com.appsoft.admin.service.entity.Payment paymentEntity = paymentEntityOpt.get();
				paymentEntity.setNetAmount(payment.getNetAmount());
				paymentEntity.setPaymentMethod(payment.getPaymentMethod());
				paymentEntity.setPaymentTime(payment.getPaymentTime());
				paymentEntity.setUserId(payment.getUserId());
				paymentEntity.setPaymentType(payment.getPaymentType());
				System.out.println(payment.getSaleList());
				paymentEntity.setSaleList(payment.getSaleList());
				
				paymentRepository.save(paymentEntity);

				return HttpStatus.OK;
			} catch (Throwable e) {
				throw new Exception("Internal server error");
			}
		}
	}

	@Override
	public HttpStatus deletePayment(Payment payment) throws ResourceNotFoundException, Exception {
		Optional<com.appsoft.admin.service.entity.Payment> paymentEntityOpt = paymentRepository.findById(payment.getPaymentId());
		if (!paymentEntityOpt.isPresent()) {
			throw new ResourceNotFoundException("Payment not found : " + payment.getPaymentId());
		} else {
			try {
				paymentRepository.delete(paymentEntityOpt.get());
				return HttpStatus.OK;
			} catch (Throwable e) {
				throw new Exception("Internal server error");
			}
		}
	}

	@Override
	public HttpStatus createPayment(Payment payment) {
		com.appsoft.admin.service.entity.Payment paymentEntity = paymentRepository.findByPaymentId(payment.getPaymentId());
		if (paymentEntity == null) {
			paymentEntity = new com.appsoft.admin.service.entity.Payment();
			paymentEntity.setNetAmount(payment.getNetAmount());
			paymentEntity.setPaymentMethod(payment.getPaymentMethod());
			paymentEntity.setPaymentTime(payment.getPaymentTime());
			paymentEntity.setUserId(payment.getUserId());
			paymentEntity.setPaymentType(payment.getPaymentType());
			paymentEntity.setSaleList(payment.getSaleList());
			
			paymentRepository.save(paymentEntity);

			return HttpStatus.OK;
		} else {
			return HttpStatus.CONFLICT;
		}
	}

}
