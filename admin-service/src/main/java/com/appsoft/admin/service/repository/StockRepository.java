package com.appsoft.admin.service.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.appsoft.admin.service.entity.Stock;

public interface StockRepository extends PagingAndSortingRepository<Stock, Integer> {
	Stock findByStockId(Integer stockId);
}
