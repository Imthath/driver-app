package com.appsoft.admin.service.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.PaymentForPurchase;
import com.appsoft.admin.service.service.PaymentForPurchaseService;

@RestController
@RequestMapping(value = "/paymentForPurchase/v1")
public class PaymentForPurchaseController {
	@Autowired
	PaymentForPurchaseService paymentForPurchaseService;
	
	@PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createPaymentForPurchase(@RequestBody PaymentForPurchase request) {
		return new ResponseEntity<PaymentForPurchase>(request, paymentForPurchaseService.createPaymentForPurchase(request));
	}

	@PutMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PaymentForPurchase> updatePaymentForPurchase(@RequestBody PaymentForPurchase request) throws ResourceNotFoundException, Exception {
		return new ResponseEntity<PaymentForPurchase>(request, paymentForPurchaseService.updatePaymentForPurchase(request));
	}

	@DeleteMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deletePaymentForPurchase(@RequestBody PaymentForPurchase request) throws ResourceNotFoundException, Exception {
		return new ResponseEntity<PaymentForPurchase>(request, paymentForPurchaseService.deletePaymentForPurchase(request));
	}

	@GetMapping(value = "/{paymentId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getPaymentForPurchaseByPaymentId(@PathVariable Integer paymentId)
			throws ResourceNotFoundException, Exception {
		return new ResponseEntity<PaymentForPurchase>(paymentForPurchaseService.getPaymentForPurchaseByPaymentId(paymentId), HttpStatus.OK);
	}

	@GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getPaymentsForPurchase(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize, @RequestParam(defaultValue = "paymentId") String sortBy)
			throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Map<String, Object>>(paymentForPurchaseService.getAllPaymentForPurchase(pageNo, pageSize, sortBy),
				HttpStatus.OK);
	}
}
