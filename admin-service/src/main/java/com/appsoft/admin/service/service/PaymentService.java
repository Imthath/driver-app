package com.appsoft.admin.service.service;

import java.util.Map;

import org.springframework.http.HttpStatus;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.Payment;
import com.appsoft.admin.service.model.Product;

public interface PaymentService {
	Payment getPaymentByPaymentId(Integer paymentId) throws ResourceNotFoundException;

	Map<String, Object> getAllPayments(Integer pageNo, Integer pageSize, String sortBy) throws ResourceNotFoundException;

	HttpStatus updatePayment(Payment payment) throws ResourceNotFoundException, Exception;

	HttpStatus deletePayment(Payment payment) throws ResourceNotFoundException, Exception;

	HttpStatus createPayment(Payment request);
}
