package com.appsoft.admin.service.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.springframework.stereotype.Service;

import com.appsoft.admin.service.model.User;
import com.appsoft.admin.service.repository.UserRepository;
import com.appsoft.admin.service.service.IQueryService;
@Service
public class QueryServiceImpl implements IQueryService{

	UserRepository userRepository;
	EntityManagerFactory emf;
	@Override
	public List<User> JPQLQuery() {
		EntityManager em = emf.createEntityManager();
		
		Query query = em.createQuery("SELECT * FROM USERS ");
		
		@SuppressWarnings("unchecked")
		List<User> list = (List<User>) query.getResultList();
		em.close();
		return list;
	}

}
