package com.appsoft.admin.service.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.Store;
import com.appsoft.admin.service.repository.StoreRepository;
import com.appsoft.admin.service.service.StoreService;
@Service
public class StoreServiceImpl implements StoreService{
	@Autowired
	private StoreRepository storeRepository;
	@Override
	public Store getStoreByStoreId(Integer storeId) throws ResourceNotFoundException {
		com.appsoft.admin.service.entity.Store storeEntity = storeRepository.findByStoreId(storeId);
		if (storeEntity == null) {
			throw new ResourceNotFoundException("Store not found : " + storeId);
		} else {
			return new Store(storeEntity.getStoreId(), storeEntity.getStoreName(), storeEntity.getStoreLocation(),
					storeEntity.getCapacity(), storeEntity.getUserId());
		}
	}

	@Override
	public Map<String, Object> getAllStore(Integer pageNo, Integer pageSize, String sortBy)
			throws ResourceNotFoundException {
		Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
		Page<com.appsoft.admin.service.entity.Store> storeEntities = storeRepository.findAll(pageable);

		if (storeEntities.hasContent()) {
			Map<String, Object> response = new HashMap<>();
			response.put("total", storeEntities.getTotalElements());
			response.put("stores", storeEntities.getContent().stream()
					.map(storeEntity -> new Store(storeEntity.getStoreId(), storeEntity.getStoreName(), storeEntity.getStoreLocation(),
							storeEntity.getCapacity(), storeEntity.getUserId()))
					.collect(Collectors.toList()));
			return response;

		} else {
			throw new ResourceNotFoundException("No any User found");
		}
	}

	@Override
	public HttpStatus updateStore(Store store) throws ResourceNotFoundException, Exception {
		Optional<com.appsoft.admin.service.entity.Store> storeEntityOpt = storeRepository.findById(store.getStoreId());
		if (!storeEntityOpt.isPresent()) {
			throw new ResourceNotFoundException("Store not found : " + store.getStoreName());
		} else {
			try {
				com.appsoft.admin.service.entity.Store storeEntity = storeEntityOpt.get();
				storeEntity.setStoreName(store.getStoreName());
				storeEntity.setStoreLocation(store.getStoreLocation());
				storeEntity.setCapacity(store.getCapacity());
				storeEntity.setUserId(store.getUserId());
				
				storeRepository.save(storeEntity);

				return HttpStatus.OK;
			} catch (Throwable e) {
				throw new Exception("Internal server error");
			}
		}
	}

	@Override
	public HttpStatus deleteStore(Store store) throws ResourceNotFoundException, Exception {
		Optional<com.appsoft.admin.service.entity.Store> storeEntityOpt = storeRepository.findById(store.getStoreId());
		if (!storeEntityOpt.isPresent()) {
			throw new ResourceNotFoundException("Store not found : " + store.getStoreName());
		} else {
			try {
				storeRepository.delete(storeEntityOpt.get());
				return HttpStatus.OK;
			} catch (Throwable e) {
				throw new Exception("Internal server error");
			}
		}
	}

	@Override
	public HttpStatus createStore(Store store) {
		com.appsoft.admin.service.entity.Store storeEntity = storeRepository.findByStoreName(store.getStoreName());
		if (storeEntity == null) {
			storeEntity = new com.appsoft.admin.service.entity.Store(store.getStoreName());
			storeEntity.setStoreName(store.getStoreName());
			storeEntity.setStoreLocation(store.getStoreLocation());
			storeEntity.setCapacity(store.getCapacity());
			storeEntity.setUserId(store.getUserId());

			storeRepository.save(storeEntity);

			return HttpStatus.OK;
		} else {
			return HttpStatus.CONFLICT;
		}
	}

	@Override
	public Store getStoreByStoreName(String storeName) throws ResourceNotFoundException {
		com.appsoft.admin.service.entity.Store storeEntity = storeRepository.findByStoreName(storeName);
		if (storeEntity == null) {
			throw new ResourceNotFoundException("Store not found : " + storeName);
		} else {
			return new Store(storeEntity.getStoreId(), storeEntity.getStoreName(), storeEntity.getStoreLocation(),
					storeEntity.getCapacity(), storeEntity.getUserId());
		}
	}

}
