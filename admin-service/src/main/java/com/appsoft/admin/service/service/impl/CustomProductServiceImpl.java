package com.appsoft.admin.service.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.entity.CustomProduct;
import com.appsoft.admin.service.repository.CustomProductRepository;
import com.appsoft.admin.service.service.CustomProductService;

@Service
public class CustomProductServiceImpl implements CustomProductService{
	@Autowired
	private CustomProductRepository customProductrepository;
	@Override
	public List<CustomProduct> findByNameEndsWith(String productName) throws ResourceNotFoundException {
		List<CustomProduct> products = (List<CustomProduct>) customProductrepository.findByNameEndsWith(productName);
        return products;
	}

}
