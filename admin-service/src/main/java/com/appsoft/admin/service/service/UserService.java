package com.appsoft.admin.service.service;

import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.User;

@Component
public interface UserService {
	User getUserByUserNameAndPassword(String userName, String password) throws ResourceNotFoundException;

	User getUserByUserName(String userName) throws ResourceNotFoundException;

	Map<String, Object> getAllUsers(Integer pageNo, Integer pageSize, String sortBy) throws ResourceNotFoundException;

	HttpStatus updateUser(User user) throws ResourceNotFoundException, Exception;

	HttpStatus deleteUser(User user) throws ResourceNotFoundException, Exception;

	HttpStatus createUser(User user);

}
