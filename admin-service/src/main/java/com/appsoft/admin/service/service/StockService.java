package com.appsoft.admin.service.service;

import java.util.Map;

import org.springframework.http.HttpStatus;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.Stock;

public interface StockService {
	Stock getStockByStockId(Integer stockId) throws ResourceNotFoundException;

	Map<String, Object> getAllStock(Integer pageNo, Integer pageSize, String sortBy) throws ResourceNotFoundException;

	HttpStatus updateStock(Stock stock) throws ResourceNotFoundException, Exception;

	HttpStatus deleteStock(Stock stock) throws ResourceNotFoundException, Exception;

	HttpStatus createStock(Stock stock);
}
