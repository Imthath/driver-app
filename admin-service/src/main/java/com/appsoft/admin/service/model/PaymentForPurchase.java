package com.appsoft.admin.service.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

public class PaymentForPurchase implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer paymentId;	
	private Double netAmount;
	private String paymentMethod;
	private Date paymentTime;
	private Integer userId;
	private String paymentType;
	private List<com.appsoft.admin.service.entity.Purchase> purchaseList;
	
	public PaymentForPurchase(Integer paymentId, Double netAmount, String paymentMethod, Date paymentTime,
			Integer userId, String paymentType, List<com.appsoft.admin.service.entity.Purchase> list) {
		this.setPaymentId(paymentId);
		this.setNetAmount(netAmount);
		this.setPaymentMethod(paymentMethod);
		this.setPaymentTime(paymentTime);
		this.setUserId(userId);
		this.setPaymentType(paymentType);
		this.setPurchaseList(list);
	}
	
	public PaymentForPurchase() {
	}

	public Integer getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(Integer paymentId) {
		this.paymentId = paymentId;
	}

	public Double getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(Double netAmount) {
		this.netAmount = netAmount;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public Date getPaymentTime() {
		return paymentTime;
	}

	public void setPaymentTime(Date paymentTime) {
		this.paymentTime = paymentTime;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public List<com.appsoft.admin.service.entity.Purchase> getPurchaseList() {
		return purchaseList;
	}

	public void setPurchaseList(List<com.appsoft.admin.service.entity.Purchase> list) {
		this.purchaseList = list;
	}
}
