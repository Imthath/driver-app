package com.appsoft.admin.service.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.Payment;
import com.appsoft.admin.service.service.PaymentService;

@RestController
@RequestMapping(value = "/payment/v1")
public class PaymentController {
	@Autowired
	PaymentService paymentService;
	
	@PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createPayment(@RequestBody Payment request) {
		return new ResponseEntity<Payment>(request, paymentService.createPayment(request));
	}

	@PutMapping(value = "/{paymentId}/update", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Payment> updatePayment(@RequestBody Payment request) throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Payment>(request, paymentService.updatePayment(request));
	}

	@DeleteMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deletePayment(@RequestBody Payment request) throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Payment>(request, paymentService.deletePayment(request));
	}

	@GetMapping(value = "/{paymentId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getPaymentByPaymentId(@PathVariable Integer paymentId)
			throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Payment>(paymentService.getPaymentByPaymentId(paymentId), HttpStatus.OK);
	}

	@GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getPayments(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize, @RequestParam(defaultValue = "paymentId") String sortBy)
			throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Map<String, Object>>(paymentService.getAllPayments(pageNo, pageSize, sortBy),
				HttpStatus.OK);
	}
}
