package com.appsoft.admin.service.service;

import java.util.List;

import com.appsoft.admin.service.model.User;

public interface IQueryService {
	List<User> JPQLQuery();

}
