package com.appsoft.admin.service.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Stock")
public class Stock {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer stockId;
	private Integer productId;
	private Double loadStockQuantity;
	private Double balanceStockQuantity;
	private Double minimumLevelQuantity;
	private String worrningFlag;
	private Integer userId;
	
	public Stock(Integer stockId) {
		this.stockId = stockId;
	}
	public Integer getStockId() {
		return stockId;
	}
	public void setStockId(Integer stockId) {
		this.stockId = stockId;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public Double getLoadStockQuantity() {
		return loadStockQuantity;
	}
	public void setLoadStockQuantity(Double loadStockQuantity) {
		this.loadStockQuantity = loadStockQuantity;
	}
	public Double getBalanceStockQuantity() {
		return balanceStockQuantity;
	}
	public void setBalanceStockQuantity(Double balanceStockQuantity) {
		this.balanceStockQuantity = balanceStockQuantity;
	}
	public Double getMinimumLevelQuantity() {
		return minimumLevelQuantity;
	}
	public void setMinimumLevelQuantity(Double minimumLevelQuantity) {
		this.minimumLevelQuantity = minimumLevelQuantity;
	}
	public String getWorrningFlag() {
		return worrningFlag;
	}
	public void setWorrningFlag(String worrningFlag) {
		this.worrningFlag = worrningFlag;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}
