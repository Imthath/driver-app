package com.appsoft.admin.service.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.Stock;
import com.appsoft.admin.service.service.ProductService;
import com.appsoft.admin.service.service.StockService;

@RestController
@RequestMapping(value = "/stock/v1")
public class StockController {
	@Autowired
	StockService stockService;

	@PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createStock(@RequestBody Stock request) {
		return new ResponseEntity<Stock>(request, stockService.createStock(request));
	}

	@PutMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Stock> updateStock(@RequestBody Stock request) throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Stock>(request, stockService.updateStock(request));
	}

	@DeleteMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteStock(@RequestBody Stock request) throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Stock>(request, stockService.deleteStock(request));
	}

	@GetMapping(value = "/{stockId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getStockByStockId(@PathVariable Integer stockId)
			throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Stock>(stockService.getStockByStockId(stockId), HttpStatus.OK);
	}

	@GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getStocks(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize, @RequestParam(defaultValue = "stockId") String sortBy)
			throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Map<String, Object>>(stockService.getAllStock(pageNo, pageSize, sortBy),
				HttpStatus.OK);
	}
}
