package com.appsoft.admin.service.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.Store;
import com.appsoft.admin.service.service.StoreService;

@RestController
@RequestMapping(value = "/store/v1")
public class StoreController {
	@Autowired
	StoreService storeService;

	@PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createStore(@RequestBody Store request) {
		return new ResponseEntity<Store>(request, storeService.createStore(request));
	}

	@PutMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Store> updateStore(@RequestBody Store request) throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Store>(request, storeService.updateStore(request));
	}

	@DeleteMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteStore(@RequestBody Store request) throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Store>(request, storeService.deleteStore(request));
	}

	@GetMapping(value = "/{storeName}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getStoreByStoreName(@PathVariable String storeName)
			throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Store>(storeService.getStoreByStoreName(storeName), HttpStatus.OK);
	}

	@GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getStores(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize, @RequestParam(defaultValue = "storeId") String sortBy)
			throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Map<String, Object>>(storeService.getAllStore(pageNo, pageSize, sortBy),
				HttpStatus.OK);
	}
}
