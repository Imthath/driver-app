package com.appsoft.admin.service.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.appsoft.admin.service.entity.PaymentForPurchase;
public interface PaymentForPurchaseRepository extends PagingAndSortingRepository<PaymentForPurchase, Integer>{
	PaymentForPurchase findByPaymentId(Integer paymentId);
}
