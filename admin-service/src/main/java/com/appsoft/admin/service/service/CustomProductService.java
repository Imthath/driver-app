package com.appsoft.admin.service.service;

import java.util.List;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.entity.CustomProduct;

public interface CustomProductService {
	List<CustomProduct> findByNameEndsWith(String productName) throws ResourceNotFoundException;

}
