package com.appsoft.admin.service.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.User;
import com.appsoft.admin.service.service.UserService;
import com.appsoft.admin.service.service.impl.QueryServiceImpl;

@RestController
@RequestMapping(value = "/user/v1")
public class UserController {
	@Autowired
	UserService userService;
	
	QueryServiceImpl gueryServiceImpl;

	@PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> getUserByUserNameAndPassword(@RequestBody User request)
			throws ResourceNotFoundException {
		User user = userService.getUserByUserNameAndPassword(request.getUserName(), request.getPassword());

		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	@PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createUser(@RequestBody User request) {
		return new ResponseEntity<User>(request, userService.createUser(request));
	}

	@PutMapping(value = "/{userName}/update", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> updateUser(@RequestBody User request) throws ResourceNotFoundException, Exception {
		return new ResponseEntity<User>(request, userService.updateUser(request));
	}

	@DeleteMapping(value = "/{userName}/delete", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteUser(@RequestBody User request) throws ResourceNotFoundException, Exception {
		return new ResponseEntity<User>(request, userService.deleteUser(request));
	}

	@GetMapping(value = "/{userName}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getUserByUserName(@PathVariable String userName)
			throws ResourceNotFoundException, Exception {
		return new ResponseEntity<User>(userService.getUserByUserName(userName), HttpStatus.OK);
	}

	@GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getUsers(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize, @RequestParam(defaultValue = "id") String sortBy)
			throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Map<String, Object>>(userService.getAllUsers(pageNo, pageSize, sortBy),
				HttpStatus.OK);
	}

}
