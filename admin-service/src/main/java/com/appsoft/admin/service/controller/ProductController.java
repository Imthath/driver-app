package com.appsoft.admin.service.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.CustomProduct;
import com.appsoft.admin.service.model.Product;
import com.appsoft.admin.service.service.CustomProductService;
import com.appsoft.admin.service.service.ProductService;

@RestController
@RequestMapping(value = "/product/v1")
public class ProductController {
	@Autowired
	ProductService productService;
	
	@Autowired
	CustomProductService customProductService;
	
	

	@PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createProduct(@RequestBody Product request) {
		return new ResponseEntity<Product>(request, productService.createProduct(request));
	}

	@PutMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Product> updateProduct(@RequestBody Product request) throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Product>(request, productService.updateProduct(request));
	}

	@DeleteMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteProduct(@RequestBody Product request) throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Product>(request, productService.deleteProduct(request));
	}

	@GetMapping(value = "/{productName}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getProductByProductName(@PathVariable String productName)
			throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Product>(productService.getProductByProductName(productName), HttpStatus.OK);
	}

	@GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getProducts(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize, @RequestParam(defaultValue = "productId") String sortBy)
			throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Map<String, Object>>(productService.getAllProducts(pageNo, pageSize, sortBy),
				HttpStatus.OK);
	}

}
