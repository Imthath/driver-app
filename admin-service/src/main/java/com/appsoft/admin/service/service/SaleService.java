package com.appsoft.admin.service.service;

import java.util.Map;

import org.springframework.http.HttpStatus;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.Sale;

public interface SaleService {
	Sale getSaleBySaleId(Integer sale) throws ResourceNotFoundException;

	Map<String, Object> getAllSale(Integer pageNo, Integer pageSize, String sortBy) throws ResourceNotFoundException;

	HttpStatus updateSale(Sale sale) throws ResourceNotFoundException, Exception;

	HttpStatus deleteSale(Sale sale) throws ResourceNotFoundException, Exception;

	HttpStatus createSale(Sale sale);
}
