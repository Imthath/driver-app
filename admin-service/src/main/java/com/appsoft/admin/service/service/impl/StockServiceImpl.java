package com.appsoft.admin.service.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.Return;
import com.appsoft.admin.service.model.Stock;
import com.appsoft.admin.service.repository.StockRepository;
import com.appsoft.admin.service.service.StockService;

@Service
public class StockServiceImpl implements StockService{
	@Autowired
	private StockRepository stockRepository;
	@Override
	public Stock getStockByStockId(Integer stockId) throws ResourceNotFoundException {
		com.appsoft.admin.service.entity.Stock stockEntity = stockRepository.findByStockId(stockId);
		if (stockEntity == null) {
			throw new ResourceNotFoundException("Stock not found : " + stockId);
		} else {
			return new Stock(stockEntity.getStockId(), stockEntity.getProductId(), stockEntity.getLoadStockQuantity(),
					stockEntity.getBalanceStockQuantity(), stockEntity.getMinimumLevelQuantity(), stockEntity.getWorrningFlag(),
					stockEntity.getUserId());
		}
	}

	@Override
	public Map<String, Object> getAllStock(Integer pageNo, Integer pageSize, String sortBy)
			throws ResourceNotFoundException {
		Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
		Page<com.appsoft.admin.service.entity.Stock> stockEntities = stockRepository.findAll(pageable);

		if (stockEntities.hasContent()) {
			Map<String, Object> response = new HashMap<>();
			response.put("total", stockEntities.getTotalElements());
			response.put("returns", stockEntities.getContent().stream()
					.map(stockEntity -> new Stock(stockEntity.getStockId(), stockEntity.getProductId(), stockEntity.getLoadStockQuantity(),
							stockEntity.getBalanceStockQuantity(), stockEntity.getMinimumLevelQuantity(), stockEntity.getWorrningFlag(),
							stockEntity.getUserId()))
					.collect(Collectors.toList()));
			return response;

		} else {
			throw new ResourceNotFoundException("No any Stock found");
		}
	}

	@Override
	public HttpStatus updateStock(Stock stock) throws ResourceNotFoundException, Exception {
		Optional<com.appsoft.admin.service.entity.Stock> stockEntityOpt = stockRepository.findById(stock.getStockId());
		if (!stockEntityOpt.isPresent()) {
			throw new ResourceNotFoundException("Stock not found : " + stock.getStockId());
		} else {
			try {
				com.appsoft.admin.service.entity.Stock stockEntity = stockEntityOpt.get();
				stockEntity.setProductId(stock.getProductId());
				stockEntity.setLoadStockQuantity(stock.getBalanceStockQuantity());
				stockEntity.setBalanceStockQuantity(stock.getBalanceStockQuantity());
				stockEntity.setMinimumLevelQuantity(stock.getMinimumLevelQuantity());
				stockEntity.setWorrningFlag(stock.getWorrningFlag());
				stockEntity.setUserId(stock.getUserId());				
				
				stockRepository.save(stockEntity);

				return HttpStatus.OK;
			} catch (Throwable e) {
				throw new Exception("Internal server error");
			}
		}
	}

	@Override
	public HttpStatus deleteStock(Stock stock) throws ResourceNotFoundException, Exception {
		Optional<com.appsoft.admin.service.entity.Stock> stockEntityOpt = stockRepository.findById(stock.getStockId());
		if (!stockEntityOpt.isPresent()) {
			throw new ResourceNotFoundException("Stock not found : " + stock.getStockId());
		} else {
			try {
				stockRepository.delete(stockEntityOpt.get());
				return HttpStatus.OK;
			} catch (Throwable e) {
				throw new Exception("Internal server error");
			}
		}
	}

	@Override
	public HttpStatus createStock(Stock stock) {
		com.appsoft.admin.service.entity.Stock stockEntity = stockRepository.findByStockId(stock.getStockId());
		if (stockEntity == null) {
			stockEntity = new com.appsoft.admin.service.entity.Stock(stock.getStockId());
			stockEntity.setProductId(stock.getProductId());
			stockEntity.setLoadStockQuantity(stock.getBalanceStockQuantity());
			stockEntity.setBalanceStockQuantity(stock.getBalanceStockQuantity());
			stockEntity.setMinimumLevelQuantity(stock.getMinimumLevelQuantity());
			stockEntity.setWorrningFlag(stock.getWorrningFlag());
			stockEntity.setUserId(stock.getUserId());

			stockRepository.save(stockEntity);

			return HttpStatus.OK;
		} else {
			return HttpStatus.CONFLICT;
		}
	}

}
