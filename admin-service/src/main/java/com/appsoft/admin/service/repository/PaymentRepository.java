package com.appsoft.admin.service.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.appsoft.admin.service.entity.Payment;
@Repository
public interface PaymentRepository extends PagingAndSortingRepository<Payment, Integer>{
	Payment findByPaymentId(Integer paymentId);
}
