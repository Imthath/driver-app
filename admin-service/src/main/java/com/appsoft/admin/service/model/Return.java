package com.appsoft.admin.service.model;

import java.io.Serializable;
import java.sql.Date;

public class Return implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer returnId;	
	private Integer productId;
	private Double price;
	private Double quantity;
	private Double amount;
	private Double discount;
	private Double netTotal;
	private String resonForReturn;
	private Integer userId;
	private Date returnTime;
	
	public Return(Integer returnId, Integer productId, Double price, Double quantity, Double amount, Double discount,
			Double netTotal, String resonForReturn, Integer userId, Date returnTime) {
		this.setReturnId(returnId);
		this.setProductId(productId);
		this.setPrice(price);
		this.setQuantity(quantity);
		this.setAmount(amount);
		this.setDiscount(discount);
		this.setNetTotal(netTotal);
		this.setResonForReturn(resonForReturn);
		this.setUserId(userId);
		this.setReturnTime(returnTime);
	}
	
	public Return() {
		
	}
	
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public Integer getReturnId() {
		return returnId;
	}
	public void setReturnId(Integer returnId) {
		this.returnId = returnId;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public Double getNetTotal() {
		return netTotal;
	}
	public void setNetTotal(Double netTotal) {
		this.netTotal = netTotal;
	}
	public String getResonForReturn() {
		return resonForReturn;
	}
	public void setResonForReturn(String resonForReturn) {
		this.resonForReturn = resonForReturn;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Date getReturnTime() {
		return returnTime;
	}
	public void setReturnTime(Date returnTime) {
		this.returnTime = returnTime;
	}
}
