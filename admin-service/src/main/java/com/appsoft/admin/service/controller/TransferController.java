package com.appsoft.admin.service.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.Transfer;
import com.appsoft.admin.service.service.TransferService;

@RestController
@RequestMapping(value = "/transfer/v1")
public class TransferController {
	@Autowired
	TransferService transferService;

	@PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createTransfer(@RequestBody Transfer request) {
		return new ResponseEntity<Transfer>(request, transferService.createTransfer(request));
	}

	@PutMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Transfer> updateTransfer(@RequestBody Transfer request) throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Transfer>(request, transferService.updateTransfer(request));
	}

	@DeleteMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteTransfer(@RequestBody Transfer request) throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Transfer>(request, transferService.deleteTransfer(request));
	}

	@GetMapping(value = "/{transferId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getTransferByTransferId(@PathVariable Integer transferId)
			throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Transfer>(transferService.getTransferByTransferId(transferId), HttpStatus.OK);
	}

	@GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getTransfers(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize, @RequestParam(defaultValue = "transferId") String sortBy)
			throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Map<String, Object>>(transferService.getAllTransfer(pageNo, pageSize, sortBy),
				HttpStatus.OK);
	}
}
