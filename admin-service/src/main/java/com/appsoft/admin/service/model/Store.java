package com.appsoft.admin.service.model;

import java.io.Serializable;

public class Store implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer storeId;
	private String storeName;
	private String storeLocation;
	private String capacity;
	private Integer userId;
	
	public Store(Integer storeId, String storeName, String storeLocation, String capacity, 
			Integer userId) {
		this.setStoreId(storeId);
		this.setStoreName(storeName);
		this.setStoreLocation(storeLocation);
		this.setCapacity(capacity);
		this.setUserId(userId);
	}
	public Store() {
		
	}
	public Integer getStoreId() {
		return storeId;
	}
	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public String getStoreLocation() {
		return storeLocation;
	}
	public void setStoreLocation(String storeLocation) {
		this.storeLocation = storeLocation;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getCapacity() {
		return capacity;
	}
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

}
