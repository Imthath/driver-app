package com.appsoft.admin.service.service;

import java.util.Map;

import org.springframework.http.HttpStatus;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.Store;

public interface StoreService {
	Store getStoreByStoreName(String storeName) throws ResourceNotFoundException;
	
	Store getStoreByStoreId(Integer storeId) throws ResourceNotFoundException;

	Map<String, Object> getAllStore(Integer pageNo, Integer pageSize, String sortBy) throws ResourceNotFoundException;

	HttpStatus updateStore(Store store) throws ResourceNotFoundException, Exception;

	HttpStatus deleteStore(Store store) throws ResourceNotFoundException, Exception;

	HttpStatus createStore(Store store);
}
