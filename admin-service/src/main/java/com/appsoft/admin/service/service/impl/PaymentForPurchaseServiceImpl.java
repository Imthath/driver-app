package com.appsoft.admin.service.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.PaymentForPurchase;
import com.appsoft.admin.service.repository.PaymentForPurchaseRepository;
import com.appsoft.admin.service.service.PaymentForPurchaseService;
@Service
public class PaymentForPurchaseServiceImpl implements PaymentForPurchaseService{
	@Autowired
	private PaymentForPurchaseRepository paymentForPurchaseRepository;
	@Override
	public PaymentForPurchase getPaymentForPurchaseByPaymentId(Integer paymentId) throws ResourceNotFoundException {
		com.appsoft.admin.service.entity.PaymentForPurchase paymentEntity = paymentForPurchaseRepository.findByPaymentId(paymentId);
		if (paymentEntity == null) {
			throw new ResourceNotFoundException("PaymentForPurchase not found : " + paymentId);
		} else {
			return new PaymentForPurchase(paymentEntity.getPaymentId(), paymentEntity.getNetAmount(), paymentEntity.getPaymentMethod(), paymentEntity.getPaymentTime(),
					paymentEntity.getUserId(), paymentEntity.getPaymentType(), paymentEntity.getPurchaseList());
		}
	}

	@Override
	public Map<String, Object> getAllPaymentForPurchase(Integer pageNo, Integer pageSize, String sortBy)
			throws ResourceNotFoundException {
		Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
		Page<com.appsoft.admin.service.entity.PaymentForPurchase> paymentEntities = paymentForPurchaseRepository.findAll(pageable);

		if (paymentEntities.hasContent()) {
			Map<String, Object> response = new HashMap<>();
			response.put("total", paymentEntities.getTotalElements());
			response.put("payments", paymentEntities.getContent().stream()
					.map(paymentEntity -> new PaymentForPurchase(paymentEntity.getPaymentId(), paymentEntity.getNetAmount(), paymentEntity.getPaymentMethod(), paymentEntity.getPaymentTime(),
							paymentEntity.getUserId(), paymentEntity.getPaymentType(), paymentEntity.getPurchaseList()))
					.collect(Collectors.toList()));
			return response;

		} else {
			throw new ResourceNotFoundException("No any PaymentForPurchase found");
		}
	}

	@Override
	public HttpStatus updatePaymentForPurchase(PaymentForPurchase paymentForPurchase)
			throws ResourceNotFoundException, Exception {
		Optional<com.appsoft.admin.service.entity.PaymentForPurchase> paymentEntityOpt = paymentForPurchaseRepository.findById(paymentForPurchase.getPaymentId());
		if (!paymentEntityOpt.isPresent()) {
			throw new ResourceNotFoundException("PaymentForPurchase not found : " + paymentForPurchase.getPaymentId());
		} else {
			try {
				com.appsoft.admin.service.entity.PaymentForPurchase paymentEntity = paymentEntityOpt.get();
				paymentEntity.setNetAmount(paymentForPurchase.getNetAmount());
				paymentEntity.setPaymentMethod(paymentForPurchase.getPaymentMethod());
				paymentEntity.setPaymentTime(paymentForPurchase.getPaymentTime());
				paymentEntity.setUserId(paymentForPurchase.getUserId());
				paymentEntity.setPaymentType(paymentForPurchase.getPaymentType());
				paymentEntity.setPurchaseList(paymentForPurchase.getPurchaseList());
				
				paymentForPurchaseRepository.save(paymentEntity);

				return HttpStatus.OK;
			} catch (Throwable e) {
				throw new Exception("Internal server error");
			}
		}
	}

	@Override
	public HttpStatus deletePaymentForPurchase(PaymentForPurchase paymentForPurchase)
			throws ResourceNotFoundException, Exception {
		Optional<com.appsoft.admin.service.entity.PaymentForPurchase> paymentEntityOpt = paymentForPurchaseRepository.findById(paymentForPurchase.getPaymentId());
		if (!paymentEntityOpt.isPresent()) {
			throw new ResourceNotFoundException("PaymentForPurchase not found : " + paymentForPurchase.getPaymentId());
		} else {
			try {
				paymentForPurchaseRepository.delete(paymentEntityOpt.get());
				return HttpStatus.OK;
			} catch (Throwable e) {
				throw new Exception("Internal server error");
			}
		}
	}

	@Override
	public HttpStatus createPaymentForPurchase(PaymentForPurchase paymentForPurchase) {
		com.appsoft.admin.service.entity.PaymentForPurchase paymentEntity = paymentForPurchaseRepository.findByPaymentId(paymentForPurchase.getPaymentId());
		if (paymentEntity == null) {
			paymentEntity = new com.appsoft.admin.service.entity.PaymentForPurchase();
			paymentEntity.setNetAmount(paymentForPurchase.getNetAmount());
			paymentEntity.setPaymentMethod(paymentForPurchase.getPaymentMethod());
			paymentEntity.setPaymentTime(paymentForPurchase.getPaymentTime());
			paymentEntity.setUserId(paymentForPurchase.getUserId());
			paymentEntity.setPaymentType(paymentForPurchase.getPaymentType());
			paymentEntity.setPurchaseList(paymentForPurchase.getPurchaseList());
			
			paymentForPurchaseRepository.save(paymentEntity);

			return HttpStatus.OK;
		} else {
			return HttpStatus.CONFLICT;
		}
	}

}
