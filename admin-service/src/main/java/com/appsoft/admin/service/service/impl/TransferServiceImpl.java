package com.appsoft.admin.service.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.Transfer;
import com.appsoft.admin.service.repository.TransferRepository;
import com.appsoft.admin.service.service.TransferService;
@Service
public class TransferServiceImpl implements TransferService{
	@Autowired
	private TransferRepository transferRepository;
	@Override
	public Transfer getTransferByTransferId(Integer transferId) throws ResourceNotFoundException {
		com.appsoft.admin.service.entity.Transfer transferEntity = transferRepository.findByTransferId(transferId);
		if (transferEntity == null) {
			throw new ResourceNotFoundException("Transfer not found : " + transferId);
		} else {
			return new Transfer(transferEntity.getTransferId(), transferEntity.getFromStoreId(), transferEntity.getToStoreId(),
					transferEntity.getTransferMedium(), transferEntity.getCost(), transferEntity.getStartTime(),
					transferEntity.getEndTime(),transferEntity.getItemDetail(),transferEntity.getUserId());
		}
	}

	@Override
	public Map<String, Object> getAllTransfer(Integer pageNo, Integer pageSize, String sortBy)
			throws ResourceNotFoundException {
		Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
		Page<com.appsoft.admin.service.entity.Transfer> transferEntities = transferRepository.findAll(pageable);

		if (transferEntities.hasContent()) {
			Map<String, Object> response = new HashMap<>();
			response.put("total", transferEntities.getTotalElements());
			response.put("transfer", transferEntities.getContent().stream()
					.map(transferEntity -> new Transfer(transferEntity.getTransferId(), transferEntity.getFromStoreId(), transferEntity.getToStoreId(),
							transferEntity.getTransferMedium(), transferEntity.getCost(), transferEntity.getStartTime(),
							transferEntity.getEndTime(),transferEntity.getItemDetail(),transferEntity.getUserId())));
			return response;

		} else {
			throw new ResourceNotFoundException("No any Transfer found");
		}
	}

	@Override
	public HttpStatus updateTransfer(Transfer transfer) throws ResourceNotFoundException, Exception {
		Optional<com.appsoft.admin.service.entity.Transfer> transferEntityOpt = transferRepository.findById(transfer.getTransferId());
		if (!transferEntityOpt.isPresent()) {
			throw new ResourceNotFoundException("Transfer not found : " + transfer.getTransferId());
		} else {
			try {
				com.appsoft.admin.service.entity.Transfer transferEntity = transferEntityOpt.get();
				transferEntity.setFromStoreId(transfer.getFromStoreId());
				transferEntity.setToStoreId(transfer.getToStoreId());
				transferEntity.setTransferMedium(transfer.getTransferMedium());
				transferEntity.setCost(transfer.getCost());
				transferEntity.setStartTime(transfer.getStartTime());
				transferEntity.setEndTime(transfer.getEndTime());
				transferEntity.setItemDetail(transfer.getItemDetail());
				transferEntity.setUserId(transfer.getUserId());
				
				transferRepository.save(transferEntity);

				return HttpStatus.OK;
			} catch (Throwable e) {
				throw new Exception("Internal server error");
			}
		}
	}

	@Override
	public HttpStatus deleteTransfer(Transfer transfer) throws ResourceNotFoundException, Exception {
		Optional<com.appsoft.admin.service.entity.Transfer> transferEntityOpt = transferRepository.findById(transfer.getTransferId());
		if (!transferEntityOpt.isPresent()) {
			throw new ResourceNotFoundException("Transfer not found : " + transfer.getTransferId());
		} else {
			try {
				transferRepository.delete(transferEntityOpt.get());
				return HttpStatus.OK;
			} catch (Throwable e) {
				throw new Exception("Internal server error");
			}
		}
	}

	@Override
	public HttpStatus createTransfer(Transfer transfer) {
		com.appsoft.admin.service.entity.Transfer transferEntity = transferRepository.findByTransferId(transfer.getTransferId());
		if (transferEntity == null) {
			transferEntity = new com.appsoft.admin.service.entity.Transfer(transfer.getTransferId());
			transferEntity.setFromStoreId(transfer.getFromStoreId());
			transferEntity.setToStoreId(transfer.getToStoreId());
			transferEntity.setTransferMedium(transfer.getTransferMedium());
			transferEntity.setCost(transfer.getCost());
			transferEntity.setStartTime(transfer.getStartTime());
			transferEntity.setEndTime(transfer.getEndTime());
			transferEntity.setItemDetail(transfer.getItemDetail());
			transferEntity.setUserId(transfer.getUserId());

			transferRepository.save(transferEntity);

			return HttpStatus.OK;
		} else {
			return HttpStatus.CONFLICT;
		}
	}

}
