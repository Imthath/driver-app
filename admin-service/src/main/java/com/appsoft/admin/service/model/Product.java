package com.appsoft.admin.service.model;

import java.io.Serializable;
import java.sql.Date;

public class Product implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer productId;	
	private String productName;	
	private Double price;	
	private String unitType;	
	private Double quantity;	
	private String brand;	
	private Double discount;	
	private String isWholeSale;	
	private Integer userId;	
	private Date updatedDateTime;
	
	public Product(Integer productId, String productName, Double price, String unitType, Double quantity, String brand,
			Double discount, String isWholeSale, Integer userId, Date updatedDateTime) {
		this.setProductId(productId);
		this.setProductName(productName);
		this.setPrice(price);
		this.setUnitType(unitType);
		this.setQuantity(quantity);
		this.setBrand(brand);
		this.setDiscount(discount);
		this.setIsWholeSale(isWholeSale);
		this.setUserId(userId);
		this.setUpdatedDateTime(updatedDateTime);
	}
	
	public Product() {		
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public String getIsWholeSale() {
		return isWholeSale;
	}

	public void setIsWholeSale(String isWholeSale) {
		this.isWholeSale = isWholeSale;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}

	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

}
