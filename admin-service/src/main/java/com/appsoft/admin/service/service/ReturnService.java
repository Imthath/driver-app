package com.appsoft.admin.service.service;

import java.util.Map;

import org.springframework.http.HttpStatus;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.Return;

public interface ReturnService {
	Return getReturnByProductName(String productName) throws ResourceNotFoundException;

	Return getReturnByReturnId(Integer returnId) throws ResourceNotFoundException;

	Map<String, Object> getAllReturn(Integer pageNo, Integer pageSize, String sortBy) throws ResourceNotFoundException;

	HttpStatus updateReturn(Return return1) throws ResourceNotFoundException, Exception;

	HttpStatus deleteReturn(Return return1) throws ResourceNotFoundException, Exception;

	HttpStatus createReturn(Return return1);
}
