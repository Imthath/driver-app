package com.appsoft.admin.service.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.appsoft.admin.service.entity.Sale;

public interface SaleRepository extends PagingAndSortingRepository<Sale, Integer> {
	Sale findBySaleId(Integer saleId);
}
