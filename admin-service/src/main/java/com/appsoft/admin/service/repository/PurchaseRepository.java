package com.appsoft.admin.service.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.appsoft.admin.service.entity.Purchase;

public interface PurchaseRepository extends PagingAndSortingRepository<Purchase, Integer> {
	Purchase findByPurchaseId(Integer purchaseId);
}
