package com.appsoft.admin.service.service.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.User;
import com.appsoft.admin.service.repository.UserRepository;
import com.appsoft.admin.service.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public User getUserByUserNameAndPassword(String userName, String password) throws ResourceNotFoundException {
		com.appsoft.admin.service.entity.User userEntity = userRepository.findByUserNameAndPassword(userName, password);
		if (userEntity == null) {
			throw new ResourceNotFoundException("User not found : " + userName);
		} else {
			return new User(userEntity.getId(), userEntity.getUserName(), userEntity.getPassword(),
					userEntity.getFirstName(), userEntity.getLastName(), userEntity.getEmail(),
					userEntity.getMobileNumber(), userEntity.getTelephone(), userEntity.getActive());
		}
	}

	@Override
	public User getUserByUserName(String userName) throws ResourceNotFoundException {
		com.appsoft.admin.service.entity.User userEntity = userRepository.findByUserName(userName);
		if (userEntity == null) {
			throw new ResourceNotFoundException("User not found : " + userName);
		} else {
			return new User(userEntity.getId(), userEntity.getUserName(), userEntity.getPassword(),
					userEntity.getFirstName(), userEntity.getLastName(), userEntity.getEmail(),
					userEntity.getMobileNumber(), userEntity.getTelephone(), userEntity.getActive());
		}
	}

	@Override
	public Map<String, Object> getAllUsers(Integer pageNo, Integer pageSize, String sortBy)
			throws ResourceNotFoundException {
		Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
		Page<com.appsoft.admin.service.entity.User> userEntities = userRepository.findAll(pageable);

		if (userEntities.hasContent()) {
			Map<String, Object> response = new HashMap<>();
			response.put("total", userEntities.getTotalElements());
			response.put("users", userEntities.getContent().stream()
					.map(userEntity -> new User(userEntity.getId(), userEntity.getUserName(), userEntity.getPassword(),
							userEntity.getFirstName(), userEntity.getLastName(), userEntity.getEmail(),
							userEntity.getMobileNumber(), userEntity.getTelephone(), userEntity.getActive()))
					.collect(Collectors.toList()));
			return response;

		} else {
			throw new ResourceNotFoundException("No any User found");
		}

	}

	@Override
	public HttpStatus updateUser(User user) throws ResourceNotFoundException, Exception {
		Optional<com.appsoft.admin.service.entity.User> userEntityOpt = userRepository.findById(user.getId());
		if (!userEntityOpt.isPresent()) {
			throw new ResourceNotFoundException("User not found : " + user.getUserName());
		} else {
			try {
				com.appsoft.admin.service.entity.User userEntity = userEntityOpt.get();
				userEntity.setUserName(user.getUserName());
				userEntity.setEmail(user.getEmail());
				userEntity.setFirstName(user.getFirstName());
				userEntity.setLastName(user.getLastName());
				userEntity.setMobileNumber(user.getMobileNumber());
				userEntity.setTelephone(user.getTelephone());
				userEntity.setActive(user.getActive());

				userRepository.save(userEntity);

				return HttpStatus.OK;
			} catch (Throwable e) {
				throw new Exception("Internal server error");
			}
		}
	}

	@Override
	public HttpStatus deleteUser(User user) throws ResourceNotFoundException, Exception {
		Optional<com.appsoft.admin.service.entity.User> userEntityOpt = userRepository.findById(user.getId());
		if (!userEntityOpt.isPresent()) {
			throw new ResourceNotFoundException("User not found : " + user.getUserName());
		} else {
			try {
				userRepository.delete(userEntityOpt.get());
				return HttpStatus.OK;
			} catch (Throwable e) {
				throw new Exception("Internal server error");
			}
		}
	}

	@Override
	public HttpStatus createUser(User user) {
		com.appsoft.admin.service.entity.User userEntity = userRepository.findByUserName(user.getUserName());
		if (userEntity == null) {
			userEntity = new com.appsoft.admin.service.entity.User(user.getUserName(), user.getPassword());
			userEntity.setEmail(user.getEmail());
			userEntity.setFirstName(user.getFirstName());
			userEntity.setLastName(user.getLastName());
			userEntity.setMobileNumber(user.getMobileNumber());
			userEntity.setTelephone(user.getTelephone());
			userEntity.setActive(user.getActive());

			userRepository.save(userEntity);

			return HttpStatus.OK;
		} else {
			return HttpStatus.CONFLICT;
		}
	}

	public String generateCommonLangPassword() {
		String upperCaseLetters = RandomStringUtils.random(2, 65, 90, true, true);
		String lowerCaseLetters = RandomStringUtils.random(2, 97, 122, true, true);
		String numbers = RandomStringUtils.randomNumeric(2);
		String specialChar = RandomStringUtils.random(2, 33, 47, false, false);
		String totalChars = RandomStringUtils.randomAlphanumeric(2);
		String combinedChars = upperCaseLetters.concat(lowerCaseLetters).concat(numbers).concat(specialChar)
				.concat(totalChars);
		List<Character> pwdChars = combinedChars.chars().mapToObj(c -> (char) c).collect(Collectors.toList());
		Collections.shuffle(pwdChars);
		String password = pwdChars.stream().collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
				.toString();
		return password;
	}

}
