package com.appsoft.admin.service.model;

import java.io.Serializable;
import java.util.Date;

public class Transfer implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer transferId;
	private Integer fromStoreId;
	private Integer toStoreId;
	private String transferMedium;
	private Double cost;
	private Date startTime;
	private Date endTime;
	private String itemDetail;
	private Integer userId;
	
	public Transfer(Integer transferId, Integer fromStoreId, Integer toStoreId, String transferMedium,
			Double cost, Date startTime, Date endTime, String itemDetail, Integer userId) {
		this.setTransferId(transferId);
		this.setFromStoreId(fromStoreId);
		this.setToStoreId(toStoreId);
		this.setTransferMedium(transferMedium);
		this.setCost(cost);
		this.setStartTime(startTime);
		this.setEndTime(endTime);
		this.setItemDetail(itemDetail);
		this.setUserId(userId);
	}
	
	public Transfer() {
		
	}
	public Integer getTransferId() {
		return transferId;
	}
	public void setTransferId(Integer transferId) {
		this.transferId = transferId;
	}
	public Integer getFromStoreId() {
		return fromStoreId;
	}
	public void setFromStoreId(Integer fromStoreId) {
		this.fromStoreId = fromStoreId;
	}
	public Integer getToStoreId() {
		return toStoreId;
	}
	public void setToStoreId(Integer toStoreId) {
		this.toStoreId = toStoreId;
	}
	public String getTransferMedium() {
		return transferMedium;
	}
	public void setTransferMedium(String transferMedium) {
		this.transferMedium = transferMedium;
	}
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getItemDetail() {
		return itemDetail;
	}
	public void setItemDetail(String itemDetail) {
		this.itemDetail = itemDetail;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}
