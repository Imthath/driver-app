package com.appsoft.admin.service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.appsoft.admin.service.entity.CustomProduct;

@Repository
public interface CustomProductRepository extends CrudRepository<CustomProduct, Integer> {
	@Query("select c from CustomProduct c where c.productName like %?1")
    List<CustomProduct> findByNameEndsWith(String productName);
}
