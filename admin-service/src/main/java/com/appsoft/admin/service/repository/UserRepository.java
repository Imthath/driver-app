package com.appsoft.admin.service.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.appsoft.admin.service.entity.User;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Integer> {
	User findByUserNameAndPassword(String userName, String password);

	User findByUserName(String userName);

	User findByEmail(String email);

}
