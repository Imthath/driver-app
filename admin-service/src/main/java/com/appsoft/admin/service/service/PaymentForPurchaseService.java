package com.appsoft.admin.service.service;

import java.util.Map;

import org.springframework.http.HttpStatus;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.PaymentForPurchase;

public interface PaymentForPurchaseService {
	PaymentForPurchase getPaymentForPurchaseByPaymentId(Integer paymentId) throws ResourceNotFoundException;

	Map<String, Object> getAllPaymentForPurchase(Integer pageNo, Integer pageSize, String sortBy) throws ResourceNotFoundException;

	HttpStatus updatePaymentForPurchase(PaymentForPurchase paymentForPurchase) throws ResourceNotFoundException, Exception;

	HttpStatus deletePaymentForPurchase(PaymentForPurchase paymentForPurchase) throws ResourceNotFoundException, Exception;

	HttpStatus createPaymentForPurchase(PaymentForPurchase paymentForPurchase);
}
