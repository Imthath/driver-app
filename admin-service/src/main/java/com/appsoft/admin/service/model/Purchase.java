package com.appsoft.admin.service.model;

import java.io.Serializable;
import java.sql.Date;


public class Purchase implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer purchaseId;		
	private Integer productId;	
	private String productName;	
	private Double price;	
	private Double quantity;	
	private Double amount;	
	private Double discount;	
	private Double netTotal;	
	private Integer userId;
	private Integer agentId;
	private Date purchaseTime;
    private PaymentForPurchase paymentForPurchase;
    
    
    public Purchase(Integer purchaseId, Integer productId, String productName, Double price, Double quantity, Double amount,
			Double discount, Double netTotal, Date purchaseTime, Integer userId, Integer agenId, PaymentForPurchase paymentForPurchase) {
		this.setPurchaseId(purchaseId);
		this.setProductId(productId);
		this.setProductName(productName);
		this.setPrice(price);
		this.setQuantity(quantity);
		this.setAmount(amount);
		this.setDiscount(discount);
		this.setNetTotal(netTotal);
		this.setPurchaseTime(purchaseTime);
		this.setUserId(userId);
		this.setAgentId(agenId);
		this.setPaymentForPurchase(paymentForPurchase);
	}
    
    public Purchase() {
    }
    
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public Double getNetTotal() {
		return netTotal;
	}
	public void setNetTotal(Double netTotal) {
		this.netTotal = netTotal;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public PaymentForPurchase getPaymentForPurchase() {
		return paymentForPurchase;
	}

	public void setPaymentForPurchase(PaymentForPurchase paymentForPurchase) {
		this.paymentForPurchase = paymentForPurchase;
	}

	public Integer getAgentId() {
		return agentId;
	}

	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}

	public Date getPurchaseTime() {
		return purchaseTime;
	}

	public void setPurchaseTime(Date purchaseTime) {
		this.purchaseTime = purchaseTime;
	}

	public Integer getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(Integer purchaseId) {
		this.purchaseId = purchaseId;
	}
	
}
