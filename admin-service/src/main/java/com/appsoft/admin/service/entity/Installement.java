package com.appsoft.admin.service.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/*Installement has been done only based on product wise*/
@Entity
@Table(name="Installement")
public class Installement {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer installementId;
	private Integer productId;
	private Double initialPayment;
	private Double monthlyPercentage;
	private Integer paymentId;
	private String installementStatus;
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public Double getInitialPayment() {
		return initialPayment;
	}
	public void setInitialPayment(Double initialPayment) {
		this.initialPayment = initialPayment;
	}
	public Double getMonthlyPercentage() {
		return monthlyPercentage;
	}
	public void setMonthlyPercentage(Double monthlyPercentage) {
		this.monthlyPercentage = monthlyPercentage;
	}
	public Integer getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(Integer paymentId) {
		this.paymentId = paymentId;
	}
	public String getInstallementStatus() {
		return installementStatus;
	}
	public void setInstallementStatus(String installementStatus) {
		this.installementStatus = installementStatus;
	}
	
	
}
