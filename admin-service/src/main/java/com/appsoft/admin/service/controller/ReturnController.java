package com.appsoft.admin.service.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.Return;
import com.appsoft.admin.service.service.ReturnService;

@RestController
@RequestMapping(value = "/return/v1")
public class ReturnController {
	@Autowired
	ReturnService returnService;

	@PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createReturn(@RequestBody Return request) {
		return new ResponseEntity<Return>(request, returnService.createReturn(request));
	}

	@PutMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Return> updateReturn(@RequestBody Return request) throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Return>(request, returnService.updateReturn(request));
	}

	@DeleteMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteReturn(@RequestBody Return request) throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Return>(request, returnService.deleteReturn(request));
	}

	@GetMapping(value = "/{returnId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getReturnByReturnId(@PathVariable Integer returnId)
			throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Return>(returnService.getReturnByReturnId(returnId), HttpStatus.OK);
	}

	@GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getReturn(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize, @RequestParam(defaultValue = "returnId") String sortBy)
			throws ResourceNotFoundException, Exception {
		return new ResponseEntity<Map<String, Object>>(returnService.getAllReturn(pageNo, pageSize, sortBy),
				HttpStatus.OK);
	}
}
