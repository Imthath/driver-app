package com.appsoft.admin.service.service;

import java.util.Map;

import org.springframework.http.HttpStatus;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.Product;

public interface ProductService {
	Product getProductByProductName(String productName) throws ResourceNotFoundException;

	Product getProductByProductId(Integer productId) throws ResourceNotFoundException;

	Map<String, Object> getAllProducts(Integer pageNo, Integer pageSize, String sortBy) throws ResourceNotFoundException;

	HttpStatus updateProduct(Product product) throws ResourceNotFoundException, Exception;

	HttpStatus deleteProduct(Product product) throws ResourceNotFoundException, Exception;

	HttpStatus createProduct(Product product);
	
}
