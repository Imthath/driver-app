package com.appsoft.admin.service.service;

import java.util.Map;

import org.springframework.http.HttpStatus;

import com.appsoft.admin.service.exception.ResourceNotFoundException;
import com.appsoft.admin.service.model.Transfer;

public interface TransferService {
	Transfer getTransferByTransferId(Integer transferId) throws ResourceNotFoundException;

	Map<String, Object> getAllTransfer(Integer pageNo, Integer pageSize, String sortBy) throws ResourceNotFoundException;

	HttpStatus updateTransfer(Transfer transfer) throws ResourceNotFoundException, Exception;

	HttpStatus deleteTransfer(Transfer transfer) throws ResourceNotFoundException, Exception;

	HttpStatus createTransfer(Transfer transfer);
}
